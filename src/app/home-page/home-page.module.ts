import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {HomePageComponent} from './home-page.component';
import {HomePageRoutingModule} from './home-page-routing.module';
import {HomePageService} from './home-page.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {Ng2SearchPipeModule} from 'ng2-search-filter';

@NgModule({
  declarations: [
    HomePageComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    HomePageRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    Ng2SearchPipeModule
  ],
  providers: [HomePageService]
})
export class HomePageModule { }
