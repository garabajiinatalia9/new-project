import {Component, OnInit} from '@angular/core';
import {Post} from '../models/post';
import {HomePageService} from './home-page.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  posts: Post[];
  post: Post;
  searchTitle: string;

  constructor(private homePageService: HomePageService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.getPosts();
  }

  openEdit(content, newPost: Post) {
    this.modalService.open(content);
    this.post = Object.assign({}, newPost);
  }

  openCreate(content) {
    this.modalService.open(content);
    this.post = new Post();
  }

  getPosts() {
    this.homePageService.getSomething().subscribe(post => this.posts = post);
  }

  createPost(title: string) {
    this.homePageService.postSomething(title).subscribe(post => this.posts.push(post));
  }

  deletePost(post: Post) {
    this.homePageService.deleteSomething(post.id).subscribe(  () => {
      const index = this.posts.indexOf(post);
      if ( index !== -1) {
       this.posts.splice(index, 1);
      }
    });
  }

  editPost() {
    this.homePageService.putSomething(this.post).subscribe( post => {
      const ix = post ? this.posts.findIndex(p => p.id === post.id) : -1;
      if (ix > -1) {
        this.posts[ix] = post;
      }
    });
  }
}
