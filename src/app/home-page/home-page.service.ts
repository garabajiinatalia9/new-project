import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Post} from '../models/post';

@Injectable()
export class HomePageService {
  private API = 'https://my-json-server.typicode.com/typicode/demo/posts';

  constructor(private http: HttpClient,
              private router: Router,
              ) {}

  getSomething(): Observable<any> {
    return this.http.get(this.API);
  }

  postSomething(newTitle): Observable<any> {
    return this.http.post(this.API, {title: newTitle});
  }

  putSomething(post: Post): Observable<any> {
    return this.http.put(this.API + `/${post.id}` , post);
  }

  deleteSomething(id: number): Observable<any> {
    return this.http.delete(this.API + `/${id}`);
  }

}
