import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomePageComponent} from './home-page.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomePageComponent}
    // { path: 'user-page', component: UserPageComponent , children: [
        // { path: '', redirectTo: 'main', pathMatch: 'full' },
        // { path: 'main', component: UserPageMainComponent },
      // ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomePageRoutingModule {}
